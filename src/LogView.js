import React, { useState } from "react";
import "antd/dist/antd.css";
import moment from "moment";
import { PostRequest } from "./Tool/HttpHelper"

import {
    Layout,
    Menu,
    Statistic,
    Row,
    Col,
    Input,
    Card,
    Typography,
    AutoComplete,
    Table,
    Space,
    Spin
} from "antd";

import {
    AppstoreOutlined,
    ArrowUpOutlined,
    ArrowDownOutlined,
    BarChartOutlined,
} from "@ant-design/icons";

const { Title } = Typography;


export class TestView extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            versions: [],
            menuview: [],
            tree: [],
            options: [],
            instructionDetails: [],
            thisVersion: ""
        };
    }


    render() {
        const { Header, Content, Footer, Sider } = Layout;
        const { Text, Link } = Typography;

        const columns = [
            {
                title: "同步版本号",
                dataIndex: "VersionNo",
                key: "VersionNo",
            },
            {
                title: "模块名称",
                dataIndex: "ModelName",
                key: "ModelName",
                width: "20%",
            },
            {
                title: "开始时间",
                dataIndex: "StartTime",
                width: "20%",
                key: "StartTime",
            },
            {
                title: "结束时间",
                dataIndex: "EndTime",
                width: "20%",
                key: "EndTime",
            },
            {
                title: "总时长(s)",
                dataIndex: "RunTimes",
                width: "20%",
                key: "RunTimes",
            },
        ];

        return (
            <div>
                <Layout
                    style={{
                        height: "100vh",
                        overflow: "hidden",
                        display: "flex",
                        "flex-direction": "column",
                    }}
                >
                    <Header className="site-layout-background" style={{ padding: 0 }}>
                        <h1 style={{ marginLeft: 20, color: "#fff" }}>
                            组织架构同步日志界面
            </h1>
                    </Header>
                    <Layout
                        className="site-layout"
                        style={{ marginLeft: 200, flex: 1, "overflow-y": "auto" }}
                    >
                        <Sider
                            style={{
                                overflow: "auto",
                                height: "100vh",
                                position: "fixed",
                                left: 0,
                            }}
                        >
                            <div>
                                <AutoComplete
                                    dropdownMatchSelectWidth={252}
                                    style={{ width: 200 }}
                                    options={this.state.options}
                                    // onSelect={onSelect}
                                    onSearch={this.handleSearch}
                                    backfill={true}
                                    notFoundContent={"无更多数据"}
                                >
                                    <Input.Search
                                        size="middle"
                                        placeholder="请输入同步版本号"
                                        enterButton
                                        onSearch={(value, event) => {
                                            this.getDataSorce(value);
                                            this.getInstructionDetails(value);
                                            // this.setState({ thisVersion: value });
                                        }}
                                    />
                                </AutoComplete>
                            </div>
                            {
                                // 渲染导航栏
                                this.state.menuview.length ? (
                                    <Menu
                                        theme="dark"
                                        mode="inline"
                                        onSelect={this.onMenuClick}
                                        defaultSelectedKeys={this.state.menuview == null ? undefined : this.state.menuview[0]}
                                    >
                                        {
                                            this.state.menuview.map((item, index) => {
                                                // console.log(this.state.menuview[0])
                                                // console.log(item);
                                                return (
                                                    <Menu.Item
                                                        key={item}
                                                        icon={<AppstoreOutlined />}
                                                    >
                                                        NO.{item}
                                                    </Menu.Item>
                                                );
                                            })
                                        }
                                    </Menu>
                                ) : null
                            }
                        </Sider>
                        <Content style={{ margin: "24px 16px 0", overflow: "initial" }}>
                            <div>
                                <Title level={4}>当前同步版本号：{this.state.thisVersion}</Title>
                            </div>
                            <div className="site-statistic-demo-card">
                                <Row gutter={16}>
                                    <Col span={6}>
                                        <Card>
                                            <Statistic
                                                title={<span style={{ fontWeight: 600 }}>新增组织</span>}
                                                value={
                                                    this.state.instructionDetails[9] == null
                                                        ? 0
                                                        : this.state.instructionDetails[9].ColumnCount
                                                }
                                                precision={0}
                                                valueStyle={{ color: "#3f8600" }}
                                                prefix={<BarChartOutlined />}
                                                suffix="个"
                                            />
                                        </Card>
                                        <Card>
                                            <Statistic
                                                title={<span style={{ fontWeight: 600 }}>变更组织</span>}
                                                value={
                                                    this.state.instructionDetails[9] == null
                                                        ? 0
                                                        : this.state.instructionDetails[1].ColumnCount +
                                                        this.state.instructionDetails[8].ColumnCount
                                                }
                                                precision={0}
                                                valueStyle={{ color: "#3f8600" }}
                                                prefix={<BarChartOutlined />}
                                                suffix="个"
                                            />
                                        </Card>
                                    </Col>
                                    <Col span={6}>
                                        <Card>
                                            <Statistic
                                                title={<span style={{ fontWeight: 600 }}>新增兼岗</span>}
                                                value={
                                                    this.state.instructionDetails[7] == null
                                                        ? 0
                                                        : this.state.instructionDetails[7].ColumnCount
                                                }
                                                precision={0}
                                                valueStyle={{ color: "#3f8600" }}
                                                prefix={<BarChartOutlined />}
                                                suffix="个"
                                            />
                                        </Card>
                                        <Card>
                                            <Statistic
                                                title={<span style={{ fontWeight: 600 }}>变更兼岗</span>}
                                                value={
                                                    this.state.instructionDetails[2] == null
                                                        ? 0
                                                        : this.state.instructionDetails[2].ColumnCount
                                                }
                                                precision={0}
                                                valueStyle={{ color: "#3f8600" }}
                                                prefix={<BarChartOutlined />}
                                                suffix="个"
                                            />
                                        </Card>
                                    </Col>
                                    <Col span={6}>
                                        <Card>
                                            <Statistic
                                                title={<span style={{ fontWeight: 600 }}>新增角色</span>}
                                                value={
                                                    this.state.instructionDetails[2] == null
                                                        ? 0
                                                        : this.state.instructionDetails[2].ColumnCount
                                                }
                                                precision={0}
                                                valueStyle={{ color: "#3f8600" }}
                                                prefix={<BarChartOutlined />}
                                                suffix="个"
                                            />
                                        </Card>
                                        <Card>
                                            <Statistic
                                                title={<span style={{ fontWeight: 600 }}>变更角色</span>}
                                                value={
                                                    this.state.instructionDetails[6] == null
                                                        ? 0
                                                        : this.state.instructionDetails[6].ColumnCount
                                                }
                                                precision={0}
                                                valueStyle={{ color: "#cf1322" }}
                                                prefix={<BarChartOutlined />}
                                                suffix="个"
                                            />
                                        </Card>
                                    </Col>
                                    <Col span={6}>
                                        <Card>
                                            <Statistic
                                                title={<span style={{ fontWeight: 600 }}>新增人员</span>}
                                                value={
                                                    this.state.instructionDetails[5] == null
                                                        ? 0
                                                        : this.state.instructionDetails[5].ColumnCount
                                                }
                                                precision={0}
                                                valueStyle={{ color: "#3f8600" }}
                                                prefix={<BarChartOutlined />}
                                                suffix="位"
                                            />
                                        </Card>
                                        <Card>
                                            <Statistic
                                                title={<span style={{ fontWeight: 600 }}>变更人员</span>}
                                                value={
                                                    this.state.instructionDetails[0] == null
                                                        ? 0
                                                        : this.state.instructionDetails[0].ColumnCount
                                                }
                                                precision={0}
                                                valueStyle={{ color: "#cf1322" }}
                                                prefix={<BarChartOutlined />}
                                                suffix="位"
                                            />
                                        </Card>
                                    </Col>
                                </Row>
                            </div>
                            <div
                                className="site-layout-background"
                                style={{ padding: 24, textAlign: "center" }}
                            >
                                <Space align="center" style={{ marginBottom: 16 }}></Space>

                                <Table columns={columns} dataSource={this.state.tree} />
                            </div>
                        </Content>
                    </Layout>
                    <Footer
                        style={{ textAlign: "center", "line-height": "30px", padding: 0 }}
                    >
                        TimesGroup ©2020
                </Footer>
                </Layout>
            </div>
        );
    }

    handleSearch = value => {
        this.setOptions(value);
    }

    setOptions = value => {
        const { menuview } = this.state;
        console.log(value);
        let reg = new RegExp(value);
        let arr = new Array();
        menuview.map((item) => {
            if (item.match(reg)) {
                let obj = { value: item };
                arr.push(obj);
            }
        });

        this.setState({ options: arr });
    }

    onMenuClick = ({ item, key, keyPath, domEvent }) => {
        this.getDataSorce(key);
        this.getInstructionDetails(key);

        this.setState({ thisVersion: key });
    };

    getMenu = async (value) => {
        let menuview = [];

        let url =
            "http://localhost/bpm/YZSoft/WebService/DataComparison.ashx?Method=GetLogVersion&UserAccount=ouwenhua";
        let requestData = {
            SysCode: "bpm",
        };

        var responseData = await PostRequest(url, requestData);
        let versions =
            responseData.data == null
                ? [{ VersionNo: 123456 }, { VersionNo: "7523523" }]
                : responseData.data.Content;

        versions.every((element, index) => {
            if (index > 9) return false;

            menuview.push(element.VersionNo);
            return true;
        });

        return menuview;
    };

    getDataSorce = async (value) => {
        let requestData = {
            VersionNo: value,
            Level: 1,
            ParteCode: "",
        };

        let url =
            "http://localhost/bpm/YZSoft/WebService/DataComparison.ashx?Method=GetLogView&UserAccount=ouwenhua";
        var result = await PostRequest(url, requestData);
        result = result.data.Content;

        console.log(result);

        let tree = [result];
        this.setState({ tree, thisVersion: value });
    };

    getInstructionDetails = async (versionNo) => {
        let url =
            "http://localhost/bpm/YZSoft/WebService/DataComparison.ashx?Method=Getzhiliang&UserAccount=ouwenhua";
        var result = await PostRequest(url, { VersionNo: versionNo });

        result = result.data.Content;

        this.setState({ instructionDetails: result });
        console.log(result);
    };

    componentDidMount = async () => {
        let menu = await this.getMenu();
        // console.log(menu);
        // console.log(menu[menu.length - 1]);
        let versionNo = menu[menu.length - 1];
        this.getDataSorce(versionNo);

        menu = menu.reverse();

        this.getInstructionDetails(versionNo);
        this.setState({
            menuview: menu,
            thisVersion: versionNo
        });
    };

    timeFormat = (time) => {
        if (time === "1900-01-01 00:00:00") return "";
        else if (time == null) return "";
        else return moment(time).format("YYYY-MM-DD HH:mm:ss");
    };
}