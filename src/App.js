import React from 'react';
import logo from './logo.svg';
import './App.css';
import { Tree } from 'antd';
import {
  DownOutlined,
  FrownOutlined,
  SmileOutlined,
  MehOutlined,
  FrownFilled,
} from '@ant-design/icons';


function App() {
  return (
    <div className="App">
      {
        <table style={{ width: 900, border: "1px", "border-color": "grey" }}>
          <tr>
            <td rowSpan="2">{<DownOutlined />}</td>
            <td>{"版本号"}</td>
            <td>{"模块名称"}</td>
            <td>{"开始时间"}</td>
            <td>{"结束时间"}</td>
            <td>{"运行时长"}</td>
            <td>{"运行结果"}</td>
          </tr>
          <tr>
            <td>{"ESB20200720001"}</td>
            <td>{"执行总时长"}</td>
            <td>{"2020-07-20 15:07:32.000"}</td>
            <td>{"2020-07-20 15:51:11.000"}</td>
            <td>{"2619"}</td>
            <td>{"成功"}</td>
            <tr>
              <td colSpan="7">
                <table>
                  <tr>
                    <td>{"版本号"}</td>
                    <td>{"模块名称"}</td>
                    <td>{"开始时间"}</td>
                    <td>{"结束时间"}</td>
                    <td>{"运行时长"}</td>
                    <td>{"运行结果"}</td>
                  </tr>
                  <tr>
                    <td>{"ESB20200720001"}</td>
                    <td>{"执行总时长"}</td>
                    <td>{"2020-07-20 15:07:32.000"}</td>
                    <td>{"2020-07-20 15:51:11.000"}</td>
                    <td>{"2619"}</td>
                    <td>{"成功"}</td>
                  </tr>
                </table>
              </td>
            </tr>
          </tr>
        </table>
      }
    </div>
  );
}

export default App;
