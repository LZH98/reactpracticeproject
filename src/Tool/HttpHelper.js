// import React from "react";
import axios from "axios";

export async function GetRequest(path, params = undefined, headers = undefined) {
    let errorMessage = "";
    let success = false;
    try {
        let queryResult = await axios.get(path, {
            params,
            headers: headers,
        });

        if (
            queryResult &&
            queryResult.data &&
            queryResult.data.StatusCode === 200
        ) {
            return {
                success: true,
                data: queryResult.data,
            };
        } else if (queryResult && queryResult.data && queryResult.data.StatusCode) {
            // 接口返回错误信息提示
            console.log(
                "接口返回错误信息提示",
                queryResult.data.ErrMsg ? queryResult.data.ErrMsg : ""
            );
            return {
                success: false,
                errorMessage: queryResult.data.ErrMsg ?
                    queryResult.data.ErrMsg : queryResult.data.Content,
            };
        } else {
            console.log("API GetRequest falied", queryResult);
            errorMessage = queryResult;
        }
    } catch (e) {
        console.log("API GetRequest error", e);
        errorMessage = e;
    }
    return {
        success,
        errorMessage,
    };
}

export async function PostRequest(path, params = undefined, headers = undefined) {
    let errorMessage = "";
    let success = false;
    try {
        let queryResult = await axios.post(path, params, {
            headers: headers
        });
        if (
            queryResult &&
            queryResult.data &&
            queryResult.data.StatusCode === 200
        ) {
            return {
                success: true,
                data: queryResult.data,
            };
        } else if (queryResult && queryResult.data && queryResult.data.StatusCode) {
            // 接口返回错误信息提示
            console.log(
                "接口返回错误信息提示",
                queryResult.data.ErrMsg ? queryResult.data.ErrMsg : ""
            );
            return {
                success: false,
                // errorMessage: queryResult.data.ErrMsg
                errorMessage: queryResult.data.ErrMsg ?
                    queryResult.data.ErrMsg : queryResult.data.Content,
            };
        } else {
            console.log("API GetRequest falied", queryResult);
            errorMessage = queryResult;
        }
    } catch (e) {
        console.log("API GetRequest error", e);
        errorMessage = e;
    }
    return {
        success,
        errorMessage,
    };
}